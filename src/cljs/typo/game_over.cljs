(ns typo.game-over
  (:require [cljs.core.async :as async]
            [taoensso.timbre :as log]
            [typo.scene :as scene]
            [typo.utils :as utils])
  (:require-macros [cljs.core.async.macros :as m :refer [go]]))

(defrecord GameOverScene [name world]
  scene/scene
  (setup [this]
    this)
  
  (key-pressed [this key key-code]
    (if (= key-code 13)
      (go (async/>! (get-in @world [:channels :scene-chan])
                    {:scene :menu
                     :name "menu"
                     :world world})))
    this)
  
  (draw [this]
    (utils/write-text-centered-at-height "Game Over!" 40 (/ js/height 2))
    (utils/write-text-centered-at-height (str "You scored " (:score @world)) 20
                                         (+ (/ js/height 2) 40))
    this)  )
