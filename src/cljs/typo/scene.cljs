(ns typo.scene)

(defprotocol scene
  "A scene within a game. The `world` is a state passed along throughout
the game"
  (setup [this])
  (key-pressed [this key key-code])
  (draw [this]))
