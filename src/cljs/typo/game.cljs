(ns typo.game
  (:require [cljs.core.async :as async]
            [clojure.string :as str]
            [taoensso.timbre :as log]
            [typo.dict-en :as dict-en]
            [typo.scene :as scene])
  (:require-macros [cljs.core.async.macros :as m :refer [go]]))

(def buffer (atom ""))
(def missed (atom 0))

(defn level-up [world]
  (if (and (> (:hits world) 0)
           (= 0 (rem (:hits world) 10)))
    (-> world
        (assoc :level (inc (:level world)))
        (assoc :hits 0))
    world))

(defn score-single [entry]
  (let [w js/width
        left (- w (:x entry))
        perc (- 100 (int (* (/ left w) 100)))]
    perc))

(defn score [matches world]
  (reduce + (map #(score-single %) matches)))

(defn check-words [ world]
  (let [w (:words world)
        b @buffer
        match (filter #(= (:text %) b) w)
        hits (count match)
        score (+ (:score world) (score match world))]

    (when (not (empty? match))
      (reset! buffer ""))

    (-> world
        (assoc :hits (+ (:hits world) hits))
        (assoc :score score)
        (assoc :words (filter #(not (= (:text %) b)) w)))))

(defn spawn-word [old-words]
  (let [n (rand-nth dict-en/words)
        y (+ 20  (rand-int  (- js/height 60)))]
    ((comp vec flatten conj) old-words {:text n :x 0 :y y})))

(defn spawn-words [ world ]
  (if (or (= 0 (rem js/frameCount 200))
          (< (count (:words world)) (:level world)))
    (assoc world :words (spawn-word (:words world)))
    world
    ))

(defn draw-word [word world]
  (let [w js/width
        curx (:x word)
        newx (+ curx (+ .5 (* .1 (:level world))))
        t (:text word)
        perc (int (* (/ (- w newx) w) 100))]

    (condp < perc
      65 (js/fill 255)
      30 (js/fill 255 255 0)
      (js/fill 255 0 0))

    (js/text t (:x word) (:y word))
    (if (< newx js/width)
      (assoc word :x newx) ;; update the word
      (do (swap! missed inc)
          nil) ;; update the missed count
      )))

(defn draw-words [world]
  (js/textSize 20)
  (assoc world :words (doall (map #(draw-word % world)
                                  (filter #(not (nil? %)) (:words world))))))

(defn draw-hud [world]
  (js/textSize 12)
  (js/text (str "Score: " (:score world)
                " Buffer: [" @buffer "]"
                " Level: " (:level world)
                " missed: " @missed
                )
           10 (- js/height 20))
  world)

(defn draw-game [world]
  (let [new-world (-> world
                      (draw-hud)
                      (draw-words)
                      (check-words)
                      (spawn-words)
                      (level-up)
                      )]
    new-world)
  )

(defrecord GameScene [name world]
  scene/scene
  (setup [this]
    (reset! buffer "")
    (reset! missed 0)
    (let [w (-> @world
                (assoc :words [])
                (assoc :level 1)
                (assoc :score 0))]
      (reset! world w)
      this))

  (key-pressed [this key key-code]
    (let [current @buffer
          backspace? (= "\b" key)
          clear? (= 13 key-code)
          adjusted (if backspace?
                     (if (not (empty? current))
                       (subs current 0 (-  (count current) 1))
                       "")
                     (str current (str/lower-case key)))]
      (if clear?
        (reset! buffer "")
        (reset! buffer adjusted)))
    this)

  (draw [this]
    (js/textSize 20)
    (if (< @missed 10)
      (let [new-world (draw-game @world)]
        (reset! world new-world)
        this)
      (do
        (go (async/>! (get-in @world [:channels :scene-chan])
                      {:scene :game-over
                       :name "game-over"
                       :world world}))
        this))))
