(ns typo.menu
  (:require [cljs.core.async :as async]
            [taoensso.timbre :as log]
            [typo.game :as game]
            [typo.scene :as scene]
            [typo.utils :as utils])
  (:require-macros [cljs.core.async.macros :as m :refer [go]]))

(defrecord MenuScene [name world]
  scene/scene

  (setup [this]
    (assoc this :world (-> @world
                           (assoc :level 0)
                           (assoc :score 0))))
  
  (key-pressed [this key key-code]
    (log/info "Scene " (:name this) " key-pressed")
    (if (= key-code 13)
      (go (async/>! (get-in @world [:channels :scene-chan]) {:scene :game
                                                             :name "game"
                                                             :world world})))
    this)
  
  (draw [this]
    (utils/write-text-centered-at-height "Typo" 40 (/ js/height 2))
    (utils/write-text-centered-at-height "Press ENTER to start" 20
                                         (+ (/ js/height 2) 40))
    this)  
  )
