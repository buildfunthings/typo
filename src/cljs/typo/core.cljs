(ns typo.core
  (:require [cljs.core.async :as async]
            [taoensso.timbre :as log]
            [typo.game :as game]
            [typo.game-over :as game-over]
            [typo.menu :as menu]
            [typo.scene :as scene])
  (:require-macros [cljs.core.async.macros :as m :refer [go]]))

(defonce game-state (atom {:channels {
                                      :scene-chan (async/chan)
                                      }
                           :level 0}))
(defonce current-scene (atom nil))

(go (while true
      (let [scene (<! (get-in @game-state [:channels :scene-chan])) 
            obj (condp = (:scene scene)
                  :menu (menu/->MenuScene (:name scene) (:world scene))
                  :game (game/->GameScene (:name scene) (:world scene))
                  :game-over (game-over/->GameOverScene (:name scene) (:world scene)))]
        (scene/setup obj)
        (reset! current-scene obj))))

(go (async/>! (get-in @game-state [:channels :scene-chan]) {:scene :menu
                                                            :name "menu"
                                                            :world game-state}))

(defn ^:export setup []
  (js/createCanvas 720 400)
  (js/textFont "monospace")

  (if (not (nil? @current-scene))
    (scene/setup @current-scene ))
  )

(defn ^:export keyPressed [key keyCode]
  (if (not (nil? @current-scene))
    (scene/key-pressed @current-scene key keyCode))
  )

(defn ^:export draw []
  (js/background 51)
  (js/fill 255)

  (if (not (nil? @current-scene))
    (scene/draw @current-scene )
    ))
