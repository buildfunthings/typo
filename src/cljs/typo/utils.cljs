(ns typo.utils)

(defn get-centerpoint-for-text [text]
  (let [s (js/textWidth text)]
    (- (/ js/width 2) (/ s 2))))

(defn write-text-centered-at-height [t size height]
  (js/textSize size)
  (let [center (get-centerpoint-for-text t)]
    (js/text t center height)))
