function setup() {
    typo.core.setup();
}

function draw() {
    typo.core.draw();    
}

function keyPressed() {
    if (keyCode !== 91) {
	typo.core.keyPressed(key, keyCode);
    }
}
