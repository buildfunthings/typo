# Typo #

This is a typing game written in ClojureScript and using the P5js library.

It is playable already, but much of the code will change over time.

To run it, just start up figwheel by running `lein figwheel` and navigate your browser to `http://localhost:3449`.
